
(function ($) {

	function setLinkPreview(fieldElement) {
		var data = fieldElement.data('picker-field');
		var attrs = $.extend({ href: data.url }, data.attributes);
		var preview = $('.link-preview', fieldElement);

		preview.empty();

		for (name in attrs) {
			$('<span></span>')
				.text(name + ": " + attrs[name])
				.css('margin-right', '1em')
				.appendTo(preview);
		}
	}

	Drupal.behaviors.picker_field = {
		attach : function(context, settings) {
			console.log(settings);

			if (settings.linkit.fields == null) {
				return false;
			}

			$.each(settings.linkit.fields, function(field_name, field) {
				$('#' + field_name, context).once('picker-field', function() {
					console.log(this);
					$this = $(this);

					$('.field-suffix', $this.parent()).remove();

					$this.data('picker-field', {
						url: $('.field-url', $this).val(),
						attributes: JSON.parse($('.field-attributes', $this).val()),
					});
					setLinkPreview($(this));

					$(".button", $(this)).click(function () {
						Drupal.settings.linkit.currentInstance.profile = Drupal.settings.linkit.fields[field_name].profile;
						Drupal.settings.linkit.currentInstance.source = field_name;
						Drupal.settings.linkit.currentInstance.helper = 'picker-field';
						Drupal.settings.linkit.currentInstance.suppressProfileChanger = true;
						Drupal.linkit.createModal();
						return false;
					});
				});
			});
		},
	};


	Drupal.linkit.registerDialogHelper('picker-field', {
		init : function() {},

		afterInit : function () {
			var source = $('#' + Drupal.settings.linkit.currentInstance.source);
			var data = source.data('picker-field');

			$('#edit-linkit-path').val(data.url);
			if (!$.isEmptyObject(data.attributes)) {
				$('#edit-linkit-attributes legend a').click();
				for (name in data.attributes) {
					$('#edit-linkit-' + name).val(data.attributes[name]);
				}
			}

			$('#linkit-modal .linkit-insert')
				.removeAttr('disabled')
				.removeClass('form-button-disabled');
		},

		insertLink : function(data) {
			console.log(data);

			for (name in data.attributes) {
				(data.attributes[name]) ? null : delete data.attributes[name];
			}

			var source = $('#' + Drupal.settings.linkit.currentInstance.source);
			source.data('picker-field', {
				url: data.path,
				attributes: data.attributes
			});
			$('.field-url', source).val(data.path);
			$('.field-attributes', source).val(JSON.stringify(data.attributes));

			if (Drupal.settings.linkit.currentInstance.linkContent !== undefined) {
				$('.field-title', source).val(Drupal.settings.linkit.currentInstance.linkContent);
			}

			setLinkPreview(source);
		},
	});

})(jQuery);
